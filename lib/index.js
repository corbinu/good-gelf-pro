'use strict';
// Load modules

const Stream = require('stream');
const Os = require('os');

const GelfPro = require('gelf-pro');

class GoodGelfPro extends Stream.Writable {
	constructor(host = '127.0.0.1', port = 12201, options = {}) {

		super({ objectMode: true});

		options.host = host;
		options.port = port;

		GelfPro.setConfig(options);
		
	}

	_write(stlog, encoding, callback) {

		let lvl = stlog.level;

		if (lvl === "warn") lvl = "warning";
		if (lvl === "log") lvl = "debug";

		let message = stlog.tags.join(", ");
		let extra = stlog.data;

		if (typeof stlog.data === 'string' || stlog.data instanceof String) {
			message = stlog.data;
			extra = {};
		} else if (typeof stlog.data.message === 'string' || stlog.data.message instanceof String) {
			message = stlog.data.message;
		}

		extra.tags = stlog.tags;
		extra.timestamp = stlog.timestamp;

		GelfPro.message(message, lvl, extra, (err) => {
			return callback(err);
		});
	}
}

module.exports = GoodGelfPro;